<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:body>
                    <w:p w14:paraId="623734DB" w14:textId="52189A76" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:pPr>
                            <w:pStyle w:val="1"/>
                            <w:jc w:val="center"/>
                        </w:pPr>
                        <w:proofErr w:type="gramStart"/>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>用于博客的</w:t>
                        </w:r>
                        <w:proofErr w:type="gramEnd"/>
                        <w:r>
                            <w:t>Word</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>模板</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="14EA7C5C" w14:textId="7D293C88" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>使用模板引擎来做生成w</w:t>
                        </w:r>
                        <w:r>
                            <w:t>ord</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>文档的优势在于：</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="79BA5A5B" w14:textId="72F994DF" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:pPr>
                            <w:ind w:firstLine="420"/>
                        </w:pPr>
                        <w:r>
                            <w:t>1</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：可视化编辑模板，可以让产品经理或者具体的业务人员制作模板。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="21BAC6D3" w14:textId="493D549A" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:pPr>
                            <w:ind w:firstLine="420"/>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>2：降低了代码量和调试难度，因为不用写代码来控制格式。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="50E37754" w14:textId="4165B41F" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="75BBE499" w14:textId="5F6E5DAF" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>缺点：</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="0E6F6047" w14:textId="713CB480" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                            </w:rPr>
                            <mc:AlternateContent>
                                <mc:Choice Requires="wps">
                                    <w:drawing>
                                        <wp:anchor distT="0" distB="0" distL="114300" distR="114300" simplePos="0"
                                                   relativeHeight="251659264" behindDoc="0" locked="0" layoutInCell="1"
                                                   allowOverlap="1" wp14:anchorId="132CD9D5" wp14:editId="76E57591">
                                            <wp:simplePos x="0" y="0"/>
                                            <wp:positionH relativeFrom="margin">
                                                <wp:align>left</wp:align>
                                            </wp:positionH>
                                            <wp:positionV relativeFrom="paragraph">
                                                <wp:posOffset>670911</wp:posOffset>
                                            </wp:positionV>
                                            <wp:extent cx="5428850" cy="3427401"/>
                                            <wp:effectExtent l="0" t="0" r="19685" b="20955"/>
                                            <wp:wrapNone/>
                                            <wp:docPr id="1" name="矩形 1"/>
                                            <wp:cNvGraphicFramePr/>
                                            <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                                <a:graphicData
                                                        uri="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
                                                    <wps:wsp>
                                                        <wps:cNvSpPr/>
                                                        <wps:spPr>
                                                            <a:xfrm>
                                                                <a:off x="0" y="0"/>
                                                                <a:ext cx="5428850" cy="3427401"/>
                                                            </a:xfrm>
                                                            <a:prstGeom prst="rect">
                                                                <a:avLst/>
                                                            </a:prstGeom>
                                                        </wps:spPr>
                                                        <wps:style>
                                                            <a:lnRef idx="2">
                                                                <a:schemeClr val="accent6"/>
                                                            </a:lnRef>
                                                            <a:fillRef idx="1">
                                                                <a:schemeClr val="lt1"/>
                                                            </a:fillRef>
                                                            <a:effectRef idx="0">
                                                                <a:schemeClr val="accent6"/>
                                                            </a:effectRef>
                                                            <a:fontRef idx="minor">
                                                                <a:schemeClr val="dk1"/>
                                                            </a:fontRef>
                                                        </wps:style>
                                                        <wps:txbx>
                                                            <w:txbxContent>
                                                                <w:p w14:paraId="44AC411D" w14:textId="42E7C848"
                                                                     w:rsidR="00303F80" w:rsidRDefault="00303F80"
                                                                     w:rsidP="00303F80">
                                                                    <w:pPr>
                                                                        <w:jc w:val="left"/>
                                                                    </w:pPr>
                                                                    <w:r>
                                                                        <w:rPr>
                                                                            <w:noProof/>
                                                                        </w:rPr>
                                                                        <w:drawing>
                                                                            <wp:inline distT="0" distB="0" distL="0"
                                                                                       distR="0"
                                                                                       wp14:anchorId="12B216AE"
                                                                                       wp14:editId="59024590">
                                                                                <wp:extent cx="1432560" cy="1432560"/>
                                                                                <wp:effectExtent l="0" t="0" r="0"
                                                                                                 b="0"/>
                                                                                <wp:docPr id="3" name="图片 3"/>
                                                                                <wp:cNvGraphicFramePr>
                                                                                    <a:graphicFrameLocks
                                                                                            xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
                                                                                            noChangeAspect="1"/>
                                                                                </wp:cNvGraphicFramePr>
                                                                                <a:graphic
                                                                                        xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                                                                    <a:graphicData
                                                                                            uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                                                        <pic:pic
                                                                                                xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                                                            <pic:nvPicPr>
                                                                                                <pic:cNvPr id="0"
                                                                                                           name="Picture 1"/>
                                                                                                <pic:cNvPicPr>
                                                                                                    <a:picLocks
                                                                                                            noChangeAspect="1"
                                                                                                            noChangeArrowheads="1"/>
                                                                                                </pic:cNvPicPr>
                                                                                            </pic:nvPicPr>
                                                                                            <pic:blipFill>
                                                                                                <a:blip r:embed="rId6">
                                                                                                    <a:extLst>
                                                                                                        <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                                                            <a14:useLocalDpi
                                                                                                                    xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main"
                                                                                                                    val="0"/>
                                                                                                        </a:ext>
                                                                                                    </a:extLst>
                                                                                                </a:blip>
                                                                                                <a:srcRect/>
                                                                                                <a:stretch>
                                                                                                    <a:fillRect/>
                                                                                                </a:stretch>
                                                                                            </pic:blipFill>
                                                                                            <pic:spPr bwMode="auto">
                                                                                                <a:xfrm>
                                                                                                    <a:off x="0" y="0"/>
                                                                                                    <a:ext cx="1432560"
                                                                                                           cy="1432560"/>
                                                                                                </a:xfrm>
                                                                                                <a:prstGeom prst="rect">
                                                                                                    <a:avLst/>
                                                                                                </a:prstGeom>
                                                                                                <a:ln>
                                                                                                    <a:noFill/>
                                                                                                </a:ln>
                                                                                                <a:effectLst>
                                                                                                    <a:softEdge
                                                                                                            rad="112500"/>
                                                                                                </a:effectLst>
                                                                                            </pic:spPr>
                                                                                        </pic:pic>
                                                                                    </a:graphicData>
                                                                                </a:graphic>
                                                                            </wp:inline>
                                                                        </w:drawing>
                                                                    </w:r>
                                                                </w:p>
                                                            </w:txbxContent>
                                                        </wps:txbx>
                                                        <wps:bodyPr rot="0" spcFirstLastPara="0" vertOverflow="overflow"
                                                                    horzOverflow="overflow" vert="horz" wrap="square"
                                                                    lIns="91440" tIns="45720" rIns="91440" bIns="45720"
                                                                    numCol="1" spcCol="0" rtlCol="0" fromWordArt="0"
                                                                    anchor="ctr" anchorCtr="0" forceAA="0"
                                                                    compatLnSpc="1">
                                                            <a:prstTxWarp prst="textNoShape">
                                                                <a:avLst/>
                                                            </a:prstTxWarp>
                                                            <a:noAutofit/>
                                                        </wps:bodyPr>
                                                    </wps:wsp>
                                                </a:graphicData>
                                            </a:graphic>
                                            <wp14:sizeRelV relativeFrom="margin">
                                                <wp14:pctHeight>0</wp14:pctHeight>
                                            </wp14:sizeRelV>
                                        </wp:anchor>
                                    </w:drawing>
                                </mc:Choice>
                                <mc:Fallback>
                                    <w:pict>
                                        <v:rect w14:anchorId="132CD9D5" id="矩形 1" o:spid="_x0000_s1026"
                                                style="position:absolute;left:0;text-align:left;margin-left:0;margin-top:52.85pt;width:427.45pt;height:269.85pt;z-index:251659264;visibility:visible;mso-wrap-style:square;mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;mso-position-horizontal:left;mso-position-horizontal-relative:margin;mso-position-vertical:absolute;mso-position-vertical-relative:text;mso-height-percent:0;mso-height-relative:margin;v-text-anchor:middle"
                                                o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#xA;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#xA;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#xA;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#xA;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#xA;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#xA;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#xA;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#xA;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#xA;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#xA;IQBqi11fdQIAABsFAAAOAAAAZHJzL2Uyb0RvYy54bWysVM1uEzEQviPxDpbvdJOQ/hB1U0WtipCq&#xA;UtGinh2v3aywPWbsZDe8DBI3HoLHQbwGY+9mW0pOiIvXs/P/zTc+PWutYRuFoQZX8vHBiDPlJFS1&#xA;eyj5x7vLVyechShcJQw4VfKtCvxs/vLFaeNnagIrMJVCRkFcmDW+5KsY/awoglwpK8IBeOVIqQGt&#xA;iCTiQ1GhaCi6NcVkNDoqGsDKI0gVAv296JR8nuNrrWR8r3VQkZmSU20xn5jPZTqL+amYPaDwq1r2&#xA;ZYh/qMKK2lHSIdSFiIKtsf4rlK0lQgAdDyTYArSupco9UDfj0bNublfCq9wLgRP8AFP4f2Hl9eYG&#xA;WV3R7DhzwtKIfn39/vPHNzZO2DQ+zMjk1t9gLwW6pkZbjTZ9qQXWZjy3A56qjUzSz8Pp5OTkkGCX&#xA;pHs9nRxPRzlq8ejuMcS3CixLl5IjDSzjKDZXIVJKMt2ZkJDK6QrIt7g1KtVg3AelqQlKOcnemT7q&#xA;3CDbCBq8kFK5eJQaonjZOrnp2pjBcbzP0cRdvb1tclOZVoPjaJ/jnxkHj5wVXBycbe0A9wWoPg2Z&#xA;O/td913Pqf3YLtt+KEuotjRGhI7fwcvLmvC8EiHeCCRC0wxoSeN7OrSBpuTQ3zhbAX7Z9z/ZE89I&#xA;y1lDC1Ly8HktUHFm3jli4JvxdJo2KgvTw+MJCfhUs3yqcWt7DjQKYhlVl6/JPprdVSPYe9rlRcpK&#xA;KuEk5S65jLgTzmO3uPQaSLVYZDPaIi/ilbv1MgVPACe+3LX3An1Pqkh8vIbdMonZM251tsnTwWId&#xA;QdeZeAniDtceetrAzJ/+tUgr/lTOVo9v2vw3AAAA//8DAFBLAwQUAAYACAAAACEAhdF61t0AAAAI&#xA;AQAADwAAAGRycy9kb3ducmV2LnhtbEyPzU7DMBCE70i8g7VI3KhTSEob4lQFVLhC+btu4yWJiNdR&#xA;7LTh7VlOcJyd1cw3xXpynTrQEFrPBuazBBRx5W3LtYHXl+3FElSIyBY7z2TgmwKsy9OTAnPrj/xM&#xA;h12slYRwyNFAE2Ofax2qhhyGme+Jxfv0g8Mocqi1HfAo4a7Tl0my0A5bloYGe7prqPrajc7AWD3c&#xA;ftT95ul+e8WP2s9X7u3dGnN+Nm1uQEWa4t8z/OILOpTCtPcj26A6AzIkyjXJrkGJvczSFai9gUWa&#xA;paDLQv8fUP4AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAA&#xA;AAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAA&#xA;AAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAaotdX3UCAAAbBQAADgAAAAAA&#xA;AAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEAhdF61t0AAAAIAQAADwAA&#xA;AAAAAAAAAAAAAADPBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAANkFAAAAAA==&#xA;"
                                                fillcolor="white [3201]" strokecolor="#70ad47 [3209]"
                                                strokeweight="1pt">
                                            <v:textbox>
                                                <w:txbxContent>
                                                    <w:p w14:paraId="44AC411D" w14:textId="42E7C848" w:rsidR="00303F80"
                                                         w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                                        <w:pPr>
                                                            <w:jc w:val="left"/>
                                                        </w:pPr>
                                                        <w:r>
                                                            <w:rPr>
                                                                <w:noProof/>
                                                            </w:rPr>
                                                            <w:drawing>
                                                                <wp:inline distT="0" distB="0" distL="0" distR="0"
                                                                           wp14:anchorId="12B216AE"
                                                                           wp14:editId="59024590">
                                                                    <wp:extent cx="1432560" cy="1432560"/>
                                                                    <wp:effectExtent l="0" t="0" r="0" b="0"/>
                                                                    <wp:docPr id="3" name="图片 3"/>
                                                                    <wp:cNvGraphicFramePr>
                                                                        <a:graphicFrameLocks
                                                                                xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
                                                                                noChangeAspect="1"/>
                                                                    </wp:cNvGraphicFramePr>
                                                                    <a:graphic
                                                                            xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                                                        <a:graphicData
                                                                                uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                                            <pic:pic
                                                                                    xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                                                                <pic:nvPicPr>
                                                                                    <pic:cNvPr id="0" name="Picture 1"/>
                                                                                    <pic:cNvPicPr>
                                                                                        <a:picLocks noChangeAspect="1"
                                                                                                    noChangeArrowheads="1"/>
                                                                                    </pic:cNvPicPr>
                                                                                </pic:nvPicPr>
                                                                                <pic:blipFill>
                                                                                    <a:blip r:embed="rId6">
                                                                                        <a:extLst>
                                                                                            <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                                                                                                <a14:useLocalDpi
                                                                                                        xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main"
                                                                                                        val="0"/>
                                                                                            </a:ext>
                                                                                        </a:extLst>
                                                                                    </a:blip>
                                                                                    <a:srcRect/>
                                                                                    <a:stretch>
                                                                                        <a:fillRect/>
                                                                                    </a:stretch>
                                                                                </pic:blipFill>
                                                                                <pic:spPr bwMode="auto">
                                                                                    <a:xfrm>
                                                                                        <a:off x="0" y="0"/>
                                                                                        <a:ext cx="1432560"
                                                                                               cy="1432560"/>
                                                                                    </a:xfrm>
                                                                                    <a:prstGeom prst="rect">
                                                                                        <a:avLst/>
                                                                                    </a:prstGeom>
                                                                                    <a:ln>
                                                                                        <a:noFill/>
                                                                                    </a:ln>
                                                                                    <a:effectLst>
                                                                                        <a:softEdge rad="112500"/>
                                                                                    </a:effectLst>
                                                                                </pic:spPr>
                                                                            </pic:pic>
                                                                        </a:graphicData>
                                                                    </a:graphic>
                                                                </wp:inline>
                                                            </w:drawing>
                                                        </w:r>
                                                    </w:p>
                                                </w:txbxContent>
                                            </v:textbox>
                                            <w10:wrap anchorx="margin"/>
                                        </v:rect>
                                    </w:pict>
                                </mc:Fallback>
                            </mc:AlternateContent>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:noProof/>
                            </w:rPr>
                            <mc:AlternateContent>
                                <mc:Choice Requires="wps">
                                    <w:drawing>
                                        <wp:anchor distT="0" distB="0" distL="114300" distR="114300" simplePos="0"
                                                   relativeHeight="251661312" behindDoc="0" locked="0" layoutInCell="1"
                                                   allowOverlap="1" wp14:anchorId="097BF6E5" wp14:editId="73BB5D6B">
                                            <wp:simplePos x="0" y="0"/>
                                            <wp:positionH relativeFrom="column">
                                                <wp:posOffset>153999</wp:posOffset>
                                            </wp:positionH>
                                            <wp:positionV relativeFrom="paragraph">
                                                <wp:posOffset>823622</wp:posOffset>
                                            </wp:positionV>
                                            <wp:extent cx="5428615" cy="3222625"/>
                                            <wp:effectExtent l="0" t="0" r="0" b="0"/>
                                            <wp:wrapNone/>
                                            <wp:docPr id="2" name="文本框 2"/>
                                            <wp:cNvGraphicFramePr/>
                                            <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                                                <a:graphicData
                                                        uri="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
                                                    <wps:wsp>
                                                        <wps:cNvSpPr txBox="1"/>
                                                        <wps:spPr>
                                                            <a:xfrm>
                                                                <a:off x="0" y="0"/>
                                                                <a:ext cx="1828800" cy="1828800"/>
                                                            </a:xfrm>
                                                            <a:prstGeom prst="rect">
                                                                <a:avLst/>
                                                            </a:prstGeom>
                                                            <a:noFill/>
                                                            <a:ln>
                                                                <a:noFill/>
                                                            </a:ln>
                                                        </wps:spPr>
                                                        <wps:txbx>
                                                            <w:txbxContent>
                                                                <w:p w14:paraId="0EFDCA9E" w14:textId="04E0824D"
                                                                     w:rsidR="00303F80" w:rsidRPr="00303F80"
                                                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                                                    <w:pPr>
                                                                        <w:jc w:val="center"/>
                                                                        <w:rPr>
                                                                            <w:b/>
                                                                            <w:noProof/>
                                                                            <w:color w:val="262626" w:themeColor="text1"
                                                                                     w:themeTint="D9"/>
                                                                            <w:sz w:val="72"/>
                                                                            <w:szCs w:val="72"/>
                                                                            <w14:shadow w14:blurRad="0" w14:dist="38100"
                                                                                        w14:dir="2700000"
                                                                                        w14:sx="100000" w14:sy="100000"
                                                                                        w14:kx="0" w14:ky="0"
                                                                                        w14:algn="bl">
                                                                                <w14:schemeClr w14:val="accent5"/>
                                                                            </w14:shadow>
                                                                            <w14:textOutline w14:w="6731" w14:cap="flat"
                                                                                             w14:cmpd="sng"
                                                                                             w14:algn="ctr">
                                                                                <w14:solidFill>
                                                                                    <w14:schemeClr w14:val="bg1"/>
                                                                                </w14:solidFill>
                                                                                <w14:prstDash w14:val="solid"/>
                                                                                <w14:round/>
                                                                            </w14:textOutline>
                                                                        </w:rPr>
                                                                    </w:pPr>
                                                                    <w:r>
                                                                        <w:rPr>
                                                                            <w:b/>
                                                                            <w:noProof/>
                                                                            <w:color w:val="262626" w:themeColor="text1"
                                                                                     w:themeTint="D9"/>
                                                                            <w:sz w:val="72"/>
                                                                            <w:szCs w:val="72"/>
                                                                            <w14:shadow w14:blurRad="0" w14:dist="38100"
                                                                                        w14:dir="2700000"
                                                                                        w14:sx="100000" w14:sy="100000"
                                                                                        w14:kx="0" w14:ky="0"
                                                                                        w14:algn="bl">
                                                                                <w14:schemeClr w14:val="accent5"/>
                                                                            </w14:shadow>
                                                                            <w14:textOutline w14:w="6731" w14:cap="flat"
                                                                                             w14:cmpd="sng"
                                                                                             w14:algn="ctr">
                                                                                <w14:solidFill>
                                                                                    <w14:schemeClr w14:val="bg1"/>
                                                                                </w14:solidFill>
                                                                                <w14:prstDash w14:val="solid"/>
                                                                                <w14:round/>
                                                                            </w14:textOutline>
                                                                        </w:rPr>
                                                                        <w:t>${articleTitle}</w:t>
                                                                    </w:r>
                                                                </w:p>
                                                            </w:txbxContent>
                                                        </wps:txbx>
                                                        <wps:bodyPr rot="0" spcFirstLastPara="0" vertOverflow="overflow"
                                                                    horzOverflow="overflow" vert="horz" wrap="none"
                                                                    lIns="91440" tIns="45720" rIns="91440" bIns="45720"
                                                                    numCol="1" spcCol="0" rtlCol="0" fromWordArt="0"
                                                                    anchor="t" anchorCtr="0" forceAA="0"
                                                                    compatLnSpc="1">
                                                            <a:prstTxWarp prst="textNoShape">
                                                                <a:avLst/>
                                                            </a:prstTxWarp>
                                                            <a:spAutoFit/>
                                                        </wps:bodyPr>
                                                    </wps:wsp>
                                                </a:graphicData>
                                            </a:graphic>
                                        </wp:anchor>
                                    </w:drawing>
                                </mc:Choice>
                                <mc:Fallback>
                                    <w:pict>
                                        <v:shapetype w14:anchorId="097BF6E5" id="_x0000_t202" coordsize="21600,21600"
                                                     o:spt="202" path="m,l,21600r21600,l21600,xe">
                                            <v:stroke joinstyle="miter"/>
                                            <v:path gradientshapeok="t" o:connecttype="rect"/>
                                        </v:shapetype>
                                        <v:shape id="文本框 2" o:spid="_x0000_s1027" type="#_x0000_t202"
                                                 style="position:absolute;left:0;text-align:left;margin-left:12.15pt;margin-top:64.85pt;width:427.45pt;height:253.75pt;z-index:251661312;visibility:visible;mso-wrap-style:none;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;mso-position-horizontal-relative:text;mso-position-vertical:absolute;mso-position-vertical-relative:text;v-text-anchor:top"
                                                 o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#xA;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#xA;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#xA;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#xA;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#xA;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#xA;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#xA;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#xA;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#xA;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#xA;IQCxOU8xMQIAAE8EAAAOAAAAZHJzL2Uyb0RvYy54bWysVMGO0zAQvSPxD5bvNGlUoERNV2VXRUjV&#xA;7kpdtGfXcZpItsey3SblA+APOHHhznf1Oxg7TbcsnBAXZzwzHs+895zZVack2QvrGtAFHY9SSoTm&#xA;UDZ6W9BPD8tXU0qcZ7pkErQo6EE4ejV/+WLWmlxkUIMshSVYRLu8NQWtvTd5kjheC8XcCIzQGKzA&#xA;KuZxa7dJaVmL1ZVMsjR9k7RgS2OBC+fQe9MH6TzWryrB/V1VOeGJLCj25uNq47oJazKfsXxrmakb&#xA;fmqD/UMXijUaLz2XumGekZ1t/iilGm7BQeVHHFQCVdVwEWfAacbps2nWNTMizoLgOHOGyf2/svx2&#xA;f29JUxY0o0QzhRQdv309fv95/PGFZAGe1rgcs9YG83z3HjqkefA7dIapu8qq8MV5CMYR6MMZXNF5&#xA;wsOhaTadphjiGBs2WD95Om6s8x8EKBKMglpkL4LK9ivn+9QhJdymYdlIGRmU+jcH1gyeJPTe9xgs&#xA;3226OOq5/w2UBxzLQq8LZ/iywatXzPl7ZlEI2C6K29/hUkloCwoni5Ia7Oe/+UM+8oNRSloUVkE1&#xA;Kp8S+VEjb+/Gk0nQYdxMXr/NcGMvI5vLiN6pa0DljvERGR7NkO/lYFYW1CO+gEW4E0NMc7y5oH4w&#xA;r30vdnxBXCwWMQmVZ5hf6bXhoXRALsD60D0ya07Ye6TtFgYBsvwZBX1uOOnMYueRiMhPQLnH9AQ+&#xA;qjYyfHph4Vlc7mPW039g/gsAAP//AwBQSwMEFAAGAAgAAAAhAEiOn/neAAAACgEAAA8AAABkcnMv&#xA;ZG93bnJldi54bWxMj8tOwzAQRfdI/IM1SOyoU7c0D+JUqMAaKHyAmwxxSDyOYrcNfD3DCpYzc3Tn&#xA;3HI7u0GccAqdJw3LRQICqfZNR62G97enmwxEiIYaM3hCDV8YYFtdXpSmaPyZXvG0j63gEAqF0WBj&#xA;HAspQ23RmbDwIxLfPvzkTORxamUzmTOHu0GqJNlIZzriD9aMuLNY9/uj05Al7rnvc/US3Pp7eWt3&#xA;D/5x/NT6+mq+vwMRcY5/MPzqszpU7HTwR2qCGDSo9YpJ3qs8BcFAluYKxEHDZpUqkFUp/1eofgAA&#xA;AP//AwBQSwECLQAUAAYACAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRl&#xA;bnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8B&#xA;AABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQCxOU8xMQIAAE8EAAAOAAAAAAAAAAAAAAAAAC4C&#xA;AABkcnMvZTJvRG9jLnhtbFBLAQItABQABgAIAAAAIQBIjp/53gAAAAoBAAAPAAAAAAAAAAAAAAAA&#xA;AIsEAABkcnMvZG93bnJldi54bWxQSwUGAAAAAAQABADzAAAAlgUAAAAA&#xA;"
                                                 filled="f" stroked="f">
                                            <v:textbox style="mso-fit-shape-to-text:t">
                                                <w:txbxContent>
                                                    <w:p w14:paraId="0EFDCA9E" w14:textId="04E0824D" w:rsidR="00303F80"
                                                         w:rsidRPr="00303F80" w:rsidRDefault="00303F80"
                                                         w:rsidP="00303F80">
                                                        <w:pPr>
                                                            <w:jc w:val="center"/>
                                                            <w:rPr>
                                                                <w:b/>
                                                                <w:noProof/>
                                                                <w:color w:val="262626" w:themeColor="text1"
                                                                         w:themeTint="D9"/>
                                                                <w:sz w:val="72"/>
                                                                <w:szCs w:val="72"/>
                                                                <w14:shadow w14:blurRad="0" w14:dist="38100"
                                                                            w14:dir="2700000" w14:sx="100000"
                                                                            w14:sy="100000" w14:kx="0" w14:ky="0"
                                                                            w14:algn="bl">
                                                                    <w14:schemeClr w14:val="accent5"/>
                                                                </w14:shadow>
                                                                <w14:textOutline w14:w="6731" w14:cap="flat"
                                                                                 w14:cmpd="sng" w14:algn="ctr">
                                                                    <w14:solidFill>
                                                                        <w14:schemeClr w14:val="bg1"/>
                                                                    </w14:solidFill>
                                                                    <w14:prstDash w14:val="solid"/>
                                                                    <w14:round/>
                                                                </w14:textOutline>
                                                            </w:rPr>
                                                        </w:pPr>
                                                        <w:r>
                                                            <w:rPr>
                                                                <w:b/>
                                                                <w:noProof/>
                                                                <w:color w:val="262626" w:themeColor="text1"
                                                                         w:themeTint="D9"/>
                                                                <w:sz w:val="72"/>
                                                                <w:szCs w:val="72"/>
                                                                <w14:shadow w14:blurRad="0" w14:dist="38100"
                                                                            w14:dir="2700000" w14:sx="100000"
                                                                            w14:sy="100000" w14:kx="0" w14:ky="0"
                                                                            w14:algn="bl">
                                                                    <w14:schemeClr w14:val="accent5"/>
                                                                </w14:shadow>
                                                                <w14:textOutline w14:w="6731" w14:cap="flat"
                                                                                 w14:cmpd="sng" w14:algn="ctr">
                                                                    <w14:solidFill>
                                                                        <w14:schemeClr w14:val="bg1"/>
                                                                    </w14:solidFill>
                                                                    <w14:prstDash w14:val="solid"/>
                                                                    <w14:round/>
                                                                </w14:textOutline>
                                                            </w:rPr>
                                                            <w:t>${articleTitle}</w:t>
                                                        </w:r>
                                                    </w:p>
                                                </w:txbxContent>
                                            </v:textbox>
                                        </v:shape>
                                    </w:pict>
                                </mc:Fallback>
                            </mc:AlternateContent>
                        </w:r>
                        <w:r>
                            <w:tab/>
                            <w:t>1</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>：每一次w</w:t>
                        </w:r>
                        <w:r>
                            <w:t>ord</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>样式调整都需要重新再次转换为模板引擎的模板。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="40E5866A" w14:textId="3D30C1A3" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80">
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>这个</w:t>
                        </w:r>
                        <w:r>
                            <w:t>Demo</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>覆盖了动态</w:t>
                        </w:r>
                        <w:r w:rsidR="00C70F06">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>带格式的</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>文字、图片、表格</w:t>
                        </w:r>
                        <w:r w:rsidR="00C70F06">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                            <w:t>。</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="2104C1C1" w14:textId="4E89D756" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="1916F05C" w14:textId="200FC567" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="1814BAF1" w14:textId="6E545DB4" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="359BBE66" w14:textId="32E41A35" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="73CE4435" w14:textId="4B1BA71B" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="7EE68867" w14:textId="6D0BD033" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="793F2D3C" w14:textId="07B7489E" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="1D698594" w14:textId="743650BC" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="62C4318F" w14:textId="1BAE1F22" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="01ED0936" w14:textId="7D871662" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="4272AF88" w14:textId="44C4BD9C" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="4AAAF545" w14:textId="1B902B9D" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="04E5EDD2" w14:textId="03E14538" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="74292419" w14:textId="695ABA1B" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="58A294B8" w14:textId="70303ACC" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="054C89CA" w14:textId="644ED097" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="3EA51243" w14:textId="5DBA7346" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="54E52396" w14:textId="7593258E" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="2914A04A" w14:textId="1755F959" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="776557F2" w14:textId="505ED8FF" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="61B78720" w14:textId="58C7C452" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="02C354A3" w14:textId="1A929256" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="510171AD" w14:textId="77777777" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="466DC18A" w14:textId="7BA6A9FD" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:p w14:paraId="43D44D77" w14:textId="42ACA021" w:rsidR="00303F80" w:rsidRDefault="00303F80"
                         w:rsidP="00303F80"/>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblStyle w:val="a5"/>
                            <w:tblW w:w="8515" w:type="dxa"/>
                            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="1"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="2838"/>
                            <w:gridCol w:w="2838"/>
                            <w:gridCol w:w="2839"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00303F80" w14:paraId="0A3BA7DD" w14:textId="77777777" w:rsidTr="00303F80">
                            <w:trPr>
                                <w:trHeight w:val="646"/>
                                <w:tblHeader/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2838" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="22BAD855" w14:textId="046FB7D2" w:rsidR="00303F80" w:rsidRPr="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00303F80">
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2838" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7509F71D" w14:textId="4E279103" w:rsidR="00303F80" w:rsidRPr="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00303F80">
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>性别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2839" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1A5A0342" w14:textId="60EBEA40" w:rsidR="00303F80" w:rsidRPr="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00303F80">
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:t>年龄</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>


                        <#list users as user>

                        <w:tr w:rsidR="00303F80" w14:paraId="1F745EEE" w14:textId="77777777" w:rsidTr="00303F80">
                            <w:trPr>
                                <w:trHeight w:val="646"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2838" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2DA37532" w14:textId="05F8AA7E" w:rsidR="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:r>
                                        <w:t>${user.name}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2838" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="72D3C392" w14:textId="65993B26" w:rsidR="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:t>${user.gender}</w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2839" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4102B206" w14:textId="399CB18A" w:rsidR="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:proofErr w:type="spellStart"/>
                                    <w:r>
                                        <w:t>${user.age}</w:t>
                                    </w:r>
                                    <w:proofErr w:type="spellEnd"/>
                                </w:p>
                            </w:tc>
                        </w:tr>
                </#list>

                        <w:tr w:rsidR="00303F80" w14:paraId="0B2A5D2C" w14:textId="77777777" w:rsidTr="00AC6658">
                            <w:trPr>
                                <w:trHeight w:val="646"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2838" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4E6AA6A1" w14:textId="0ACF5E3C" w:rsidR="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>第一行为标题行，同时设置了标题行跨页重复出现</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5677" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="1C47D7B1" w14:textId="2E450614" w:rsidR="00303F80" w:rsidRPr="00303F80"
                                     w:rsidRDefault="00303F80" w:rsidP="00303F80">
                                    <w:pPr>
                                        <w:jc w:val="right"/>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:i/>
                                            <w:iCs/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00303F80">
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:i/>
                                            <w:iCs/>
                                        </w:rPr>
                                        <w:t>https://blog.csdn.net/wangxudongx</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="7387D9EB" w14:textId="77777777" w:rsidR="00303F80" w:rsidRPr="00303F80"
                         w:rsidRDefault="00303F80" w:rsidP="00303F80"/>
                    <w:sectPr w:rsidR="00303F80" w:rsidRPr="00303F80">
                        <w:footerReference w:type="default" r:id="rId7"/>
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId7"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"
                              Target="footer1.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
                              Target="media/image1.png"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
                <Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                         xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:footnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="68AA84F3" w14:textId="77777777" w:rsidR="00877557" w:rsidRDefault="00877557"/>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="5A505DF7" w14:textId="77777777" w:rsidR="00877557" w:rsidRDefault="00877557"/>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:endnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="04D874D1" w14:textId="77777777" w:rsidR="00877557" w:rsidRDefault="00877557"/>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="2B0A5F1C" w14:textId="77777777" w:rsidR="00877557" w:rsidRDefault="00877557"/>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footer1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml">
        <pkg:xmlData>
            <w:ftr xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                   xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                   xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                   xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                   xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                   xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                   xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                   xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                   xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                   xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                   xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                   xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                   xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                   xmlns:o="urn:schemas-microsoft-com:office:office"
                   xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                   xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                   xmlns:v="urn:schemas-microsoft-com:vml"
                   xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                   xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                   xmlns:w10="urn:schemas-microsoft-com:office:word"
                   xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                   xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                   xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                   xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                   xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                   xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                   xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                   xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                   xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                   xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                   xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                   xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                   mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:sdt>
                    <w:sdtPr>
                        <w:id w:val="-208500195"/>
                        <w:docPartObj>
                            <w:docPartGallery w:val="Page Numbers (Bottom of Page)"/>
                            <w:docPartUnique/>
                        </w:docPartObj>
                    </w:sdtPr>
                    <w:sdtContent>
                        <w:sdt>
                            <w:sdtPr>
                                <w:id w:val="-1769616900"/>
                                <w:docPartObj>
                                    <w:docPartGallery w:val="Page Numbers (Top of Page)"/>
                                    <w:docPartUnique/>
                                </w:docPartObj>
                            </w:sdtPr>
                            <w:sdtContent>
                                <w:p w14:paraId="1696E9A0" w14:textId="69D70AE3" w:rsidR="00FE28E7"
                                     w:rsidRDefault="00FE28E7">
                                    <w:pPr>
                                        <w:pStyle w:val="a8"/>
                                        <w:jc w:val="right"/>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="begin"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:instrText>PAGE</w:instrText>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="separate"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>2</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="end"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> / </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="begin"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                        </w:rPr>
                                        <w:instrText>NUMPAGES</w:instrText>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="separate"/>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:lang w:val="zh-CN"/>
                                        </w:rPr>
                                        <w:t>2</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:b/>
                                            <w:bCs/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:fldChar w:fldCharType="end"/>
                                    </w:r>
                                </w:p>
                            </w:sdtContent>
                        </w:sdt>
                    </w:sdtContent>
                </w:sdt>
                <w:p w14:paraId="47269E4F" w14:textId="77777777" w:rsidR="00FE28E7" w:rsidRDefault="00FE28E7">
                    <w:pPr>
                        <w:pStyle w:val="a8"/>
                    </w:pPr>
                </w:p>
            </w:ftr>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/media/image1.png" pkg:contentType="image/png" pkg:compression="store">
        <pkg:binaryData>${demoImage}
        </pkg:binaryData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:zoom w:percent="90"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2050"/>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="-1"/>
                    <w:footnote w:id="0"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="-1"/>
                    <w:endnote w:id="0"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="15"/>
                    <w:compatSetting w:name="overrideTableStyleFontSizeAndJustification"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="enableOpenTypeFeatures" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="doNotFlipMirrorIndents" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="differentiateMultirowTableHeaders"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="0"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00303F80"/>
                    <w:rsid w:val="00303F80"/>
                    <w:rsid w:val="00877557"/>
                    <w:rsid w:val="00C70F06"/>
                    <w:rsid w:val="00D01D85"/>
                    <w:rsid w:val="00F30D9D"/>
                    <w:rsid w:val="00FE28E7"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2050"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="2"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="6E6EFB1A"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{14B6361B-D0E8-418D-9C32-2AE92886E1E2}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                      xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                      xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                      xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                      xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                      mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia"
                                      w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="376">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="39"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="1">
                    <w:name w:val="heading 1"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:link w:val="10"/>
                    <w:uiPriority w:val="9"/>
                    <w:qFormat/>
                    <w:rsid w:val="00303F80"/>
                    <w:pPr>
                        <w:keepNext/>
                        <w:keepLines/>
                        <w:spacing w:before="340" w:after="330" w:line="578" w:lineRule="auto"/>
                        <w:outlineLvl w:val="0"/>
                    </w:pPr>
                    <w:rPr>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="44"/>
                        <w:sz w:val="44"/>
                        <w:szCs w:val="44"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="10">
                    <w:name w:val="标题 1 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="1"/>
                    <w:uiPriority w:val="9"/>
                    <w:rsid w:val="00303F80"/>
                    <w:rPr>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="44"/>
                        <w:sz w:val="44"/>
                        <w:szCs w:val="44"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="Title"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:link w:val="a4"/>
                    <w:uiPriority w:val="10"/>
                    <w:qFormat/>
                    <w:rsid w:val="00303F80"/>
                    <w:pPr>
                        <w:spacing w:before="240" w:after="60"/>
                        <w:jc w:val="center"/>
                        <w:outlineLvl w:val="0"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia" w:hAnsiTheme="majorHAnsi"
                                  w:cstheme="majorBidi"/>
                        <w:b/>
                        <w:bCs/>
                        <w:sz w:val="32"/>
                        <w:szCs w:val="32"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a4">
                    <w:name w:val="标题 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a3"/>
                    <w:uiPriority w:val="10"/>
                    <w:rsid w:val="00303F80"/>
                    <w:rPr>
                        <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsiaTheme="majorEastAsia" w:hAnsiTheme="majorHAnsi"
                                  w:cstheme="majorBidi"/>
                        <w:b/>
                        <w:bCs/>
                        <w:sz w:val="32"/>
                        <w:szCs w:val="32"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="table" w:styleId="a5">
                    <w:name w:val="Table Grid"/>
                    <w:basedOn w:val="a1"/>
                    <w:uiPriority w:val="39"/>
                    <w:rsid w:val="00303F80"/>
                    <w:tblPr>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                    </w:tblPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a6">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a7"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="00FE28E7"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:sz="6" w:space="1" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a7">
                    <w:name w:val="页眉 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a6"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="00FE28E7"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a8">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a9"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="00FE28E7"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a9">
                    <w:name w:val="页脚 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a8"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="00FE28E7"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                           xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                           xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                           xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                           xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                           mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:optimizeForBrowser/>
                <w:allowPNG/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                     xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                     xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                     xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                     xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                     mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>王 旭东</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>王 旭东</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2021-10-10T08:24:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2021-10-10T08:24:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>1</TotalTime>
                <Pages>1</Pages>
                <Words>42</Words>
                <Characters>242</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>2</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>283</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>
