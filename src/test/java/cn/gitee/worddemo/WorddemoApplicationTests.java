package cn.gitee.worddemo;

import cn.gitee.worddemo.util.WordUtil;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.*;

@SpringBootTest
class WorddemoApplicationTests {

    @Test
    void contextLoads() {
    }
    @Autowired
    private WordUtil wordUtil;
    //    public static void main(String[] args) {
    @Test
    public void testProc(){

        String userDir = System.getProperty("user.dir");
        System.out.println(userDir);
        String fileName = "/word/demo/test.doc"; // 输出word文件名字
        String ftlName = "forBlogWordTemplate.ftl"; // 使用的模板文件


        Map<String, Object> dataMap = new HashMap<String, Object>();
        List<Map<String,Object>> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", "一个测试特使");
            map.put("age", 234);
            map.put("gender", "女");

            list.add(map);
        }
        dataMap.put("users",list);
        dataMap.put("articleTitle","人间正道是沧桑");
        String demoImageBase64Str = wordUtil.getImgBase64ByUrl("http://image11.m1905.cn/uploadfile/2013/1118/thumb_1_224_224_20131118051600573973.jpg");
        System.out.println(demoImageBase64Str);

        dataMap.put("demoImage",demoImageBase64Str);

        File file = wordUtil.create(dataMap, ftlName, fileName);
        System.out.println(file.getName());
    }


    @Test
    public void testProc2(){

        String userDir = System.getProperty("user.dir");
        System.out.println(userDir);
        String fileName = "/word/demo/test2.doc"; // 输出word文件名字
        String ftlName = "solution2.ftl"; // 使用的模板文件

        Map<String, Object> tables = new LinkedHashMap<>();

        List<String> columns = new LinkedList<>();
        columns.add("姓名");
        columns.add("性别");
        columns.add("年龄");
        columns.add("身高");

        List<Map<String, Object>> rows = new LinkedList<>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", "张三");
        map.put("age", 24);
        map.put("gender", "女");
        map.put("height", 178);

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("name", "李四");
        map2.put("age", 23);
        map2.put("gender", "男");
        map2.put("height", 179);


        Map<String, Object> map3 = new HashMap<String, Object>();
        map3.put("name", "一个测试特使");
        map3.put("age", 24);
        map3.put("gender", "女");
        map3.put("height", 178);


        Map<String, Object> map4 = new HashMap<String, Object>();
        map4.put("name", "一个测试22");
        map4.put("age", 34);
        map4.put("gender", "男");
        map4.put("height", 178);

        Map<String, Object> map5 = new HashMap<String, Object>();
        map5.put("percent", "2/2");

        rows.add(map);
        rows.add(map2);
        rows.add(map3);
        rows.add(map4);
        rows.add(map5);


        tables.put("columns", columns);
        tables.put("rows", rows);


        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("complexTable", tables);

        dataMap.put("articleTitle","人间正道是沧桑");
        String demoImageBase64Str = wordUtil.getImgBase64ByUrl("http://image11.m1905.cn/uploadfile/2013/1118/thumb_1_224_224_20131118051600573973.jpg");
        System.out.println(demoImageBase64Str);

        dataMap.put("demoImage",demoImageBase64Str);

        File file = wordUtil.create(dataMap, ftlName, fileName);
        System.out.println(file.getName());
    }
}
